#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <ctime>

// function to generate ASCII art based on a mathematical function
void generateASCIIArt(int width, int height) {
	const char* grayscale = "@%#*+=-:. ";
	const int numShades = strlen(grayscale);

	//TODO: assign ANSI code colors to grayscale values

	// generate rand nums for axis values
	const int a = 2; // num of axis 
	int axis[a]; // array for x and y axis vals
	for (int i = 0; i < a; ++i) {	
		srand(time(0) + i); // new seed for each
		axis[i] = rand() % 10 + 1; // assign rand num for axis; 10-1
	}	

	for (int y = 0; y < height; ++y) {
       		for (int x = 0; x < width; ++x) {
	
	    	// calculate a value based on position (e.g., a mathematical function)
            	double value = sin(x / axis[0]) + cos(y / axis[1]);    

            	// map the value to a character in the grayscale
            	int index = static_cast<int>((value + 2.0) / 4.0 * (numShades - 1));
            	index = std::max(0, std::min(index, numShades - 1)); // Clamp index
            
            	// output the character to the terminal
           	std::cout << grayscale[index];
        	}
		std::cout << std::endl;
	}
}

int main() {
	generateASCIIArt(80,20); // gen random 40x80 ascii art

	return 0;
}

